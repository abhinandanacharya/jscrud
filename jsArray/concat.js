/**
 * Array Function
 * 1. Concat()
 *  defination = this method is used to join two or more array and return a new array(result array)
 *  syntax = array1.concat(array2,array3....arrayN)
 *  you can use it for same as string to concat the two string
 */

 var arr = ["my","name","is"];
 var arr2 = ["abhinandan","Acharya"];

 var full = arr.concat(arr2);
 document.write(full);
 var str1 = "abhinandan";
 var str2 = "acharya";
 var str3 = str1.concat(str2);
 document.write(str3);