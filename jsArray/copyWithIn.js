/**
 * Array Function
 * 2. copyWithIn()
 *  Defination = copyWithIn() method copies array element to another position in the same array , 
 *               overwrting the existng values
 *  syntax = array.copyWithIn(target,start,end)
 *           
 */

 var arr = ['a','b','c','d'];
 document.write(arr.copyWithin(2,3));
//  var x;
//   for(f of arr)
//   {
//       document.write(arr[x]);
//   }

//  var str = "abhinandan";
//  document.write(str.copyWithin(2,4));
