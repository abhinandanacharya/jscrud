/**
 * Array Function
 * 4. every()
 *  defination = the every() methods checks if all element in an arry pass a test
 *               test is provided as a function
 *  syntax = array.every(function(currentValue,index,arr),thisValue)
 *  
 */

var arr = [1,2,3,4,6,7,8,9,10];
checkNumExist = (num) => num<0;
document.write(arr.every(checkNumExist));
//  var str = "abhinandan";
//  checkChar = (ch) => ch;
//  document.write(str.every(checkChar));