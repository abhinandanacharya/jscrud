/**
 * Array Function
 * 8. findIndex()
 * defination = findIndex() return the index of the first element that pass the test
 *              test is provided as function
 * syntax = array.findIndex(function(value,index,arr),thisValue)
 */

 var arr = [12,34,56,22,12];
findIndexArr = (keyId) => keyId==12;
document.write(arr.findIndex(findIndexArr));