/**
 * Array function 
 * 7. find()
 * defination = return the first value of an array elementthat pass the test.
 *              test provided as function.
 *              it does't change the original array
 *              it does't not execute an empty array
 * syntax = array.find(function(value,index,arr),thisValue)
 */

 var arr = [12,23,45,56,67,89];
checkAge = (age) => age>18;
document.write(arr.find(checkAge));
