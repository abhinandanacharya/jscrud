/**
 * Array Function
 * 6. filter()
 * defination = The filter() cmethod creates an array and filled with all array elements that pass a test
 *              test is provided as a function
 *              it does't change the original array
 * syntax = array.filter(function(value,index,arr),thisValue)
 * 
 */
var arr = ['abhi','body','age','ds','tm'];
//var ch = 'body';
checkCharFilter = (ch) => ch == 'body';

document.write(arr.filter(checkCharFilter));