/**
 * Array function 
 * 5. fill()
 * defination = the fill() method fills the specified elements in an array with a static value
 *              you can specife the postion(start and end)
 *              this method overwright orignal array
 * syntax = array.fill(value,start,end)
 * 
 */

 var arr = ['abhi',22,'name',20];
 document.write(arr.fill('age'));